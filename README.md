![ubuntu 22.04](https://img.shields.io/badge/-Linux-grey?logo=linux)![Bash Shell](https://badges.frapsoft.com/bash/v1/bash.png?v=103)![python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)

## Introduction

### User story

As the founder of An Exchange Company, I want to establish a system that periodically retrieves exchange rate data from USD to 3 of my preferred currencies ( EUR, CAD, ZAR) and exports it as a JSON output to an S3 bucket.

## Steps

1. create S3 bucket
2. launch an ec2 instance

    we used ubuntu 22.04 

    + install AWS CLI
    + install python 
    + install forex-python module ( Documentaion: https://pypi.org/project/forex-python/ )
3. write the code

    + write the python code to generate the rates
    + write the bash code to execute the python code, write in a JSONfile and send it to S3
4. set up cron job

```python
import forex_python

from forex_python.converter import CurrencyRates
c = CurrencyRates()

print(c.get_rate('USD', 'EUR'))
print(c.get_rate('USD', 'CAD'))
print(c.get_rate('USD', 'ZAR'))
```

```bash
#!/bin/bash

#create the file with current date.
current_date=$(date '+%Y-%m-%d-%H-%M-%S')
touch $current_date-data.json

#Generate exchange rate and save it in the file.
python3 exchange.py > $current_date-data.json

#Upload file to AWS s3.
aws s3 cp $current_date-data.json s3://exchange-p1/rates/
```

   - Configure a cron job to run `run_exchange.sh` daily at a specific time (modify paths and time as needed). Edit the crontab file:

```bash
crontab -e
```

   - Add the following line to run the script daily (e.g., at 9:00 AM):

```bash
0 9 * * * /bin/bash /path/to/exchange/scripts/run_exchange.sh
```





