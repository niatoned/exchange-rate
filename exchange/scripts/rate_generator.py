import forex_python
#import time
import json
#import boto3

from forex_python.converter import CurrencyRates
#from secrets import access_key, secret_access_key
c = CurrencyRates()
#print(c.get_rates('USD'))
#filename = "rates"+time.strftime("%Y%m%d-%H%M%S")+".json"
rates = {
    "EUR": c.get_rate("USD","EUR"),
    "CAD": c.get_rate("USD","CAD"),
    "ZAR": c.get_rate("USD","ZAR")
}
json_object = json.dumps(rates, indent=4)
#with open("/home/ubuntu/exchange/data_output/"+filename, 'w') as f:
#    f.write(json_object)
#    f.write("exit")

#client = boto3.client('s3')

#client.upload_file(
#    Filename='/home/ubuntu/exchange/data_output/'+filename,
#    Bucket='rate-exchange-bucket',
#    Key='rates/'+filename
#)
