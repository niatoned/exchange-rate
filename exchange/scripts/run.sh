#!/bin/bash

current_date=$(date '+%Y-%m-%d-%H-%M-%S')
touch /home/ubuntu/exchange-rate/exchange/data_output/$current_date-data.json
python3 rate_generator.py > /home/ubuntu/exchange-rate/exchange/data_output/$current_date-data.json
aws s3 cp /home/ubuntu/exchange-rate/exchange/data_output/$current_date-data.json s3://rate-exchange-bucket/rates/


